import React from 'react';
import Logo from "../logo.svg";
import Button from "../atom/Button";
import {Link} from "react-router-dom";

export default function Header(props) {
    return (
        <header className="site-header">
				<div className="container">
					<div className="site-header-inner">
						<div className="brand">
							<h1 className="m-0">
                                <Link to="/"><img
										src={Logo}
										alt="Cube"
										width="32"
										height="32"
								/></Link>
							</h1>
						</div>
						<button
							id="header-nav-toggle"
							className="header-nav-toggle"
							aria-controls="primary-menu"
							aria-expanded="false"
						>
							<span className="screen-reader">Menu</span>
							<span className="hamburger"
								><span className="hamburger-inner"></span
							></span>
						</button>
						<nav id="header-nav" className="header-nav">
							<div className="header-nav-inner">
								<ul
									className="list-reset text-xxs header-nav-right"
								>
									<li><Link to="#">Home</Link></li>
									<li><Link to="#">Menu 1</Link></li>
									<li><Link to="#">Menu 2</Link></li>
									<li><Link to="#">Menu 3</Link></li>
								</ul>
								<ul className="list-reset header-nav-right">
									<li>
                                        <Button isPrimary isSmall>Sign Up</Button>
										
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
			</header>
    )
}