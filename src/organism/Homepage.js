import React from "react";

import MainImage from "assets/images/landingpage.jpg";
import FeatureTile01 from "assets/images/feature-tile-icon-01.svg";
import FeatureTile02 from "assets/images/feature-tile-icon-02.svg";
import FeatureTile03 from "assets/images/feature-tile-icon-03.svg";

import Section from "atom/Section";
import Header from "molecule/Header";
import Features from "molecule/Features";
import Footer from "molecule/Footer";
import Fade from "react-reveal/Fade";

export default function Homepage() {
  const features = [
    {
      imgSrc: FeatureTile01,
      imgAlt: "Feature tile icon 01",
      title: "Card 1",
      description:
        "A pseudo-Latin text used in web design, layout, and printing in place of things to emphasise design",
    },
    {
      imgSrc: FeatureTile02,
      imgAlt: "Feature tile icon 02",
      title: "Card 2",
      description:
        "A pseudo-Latin text used in web design, layout, and printing in place of things to emphasise design",
    },
    {
      imgSrc: FeatureTile03,
      imgAlt: "Feature tile icon 03",
      title: "Card 3",
      description:
        "A pseudo-Latin text used in web design, layout, and printing in place of things to emphasise design",
    },
  ];

  
  return (
    <div className="body-wrap">
      <Header></Header>
      <main className="site-content">
        <Section className="hero illustration-section-01" isCenteredContent>
          <div className="container-sm">
            <div className="hero-inner section-inner">
              <div className="hero-content">
                <h1 className="mt-0 mb-16">
                  <Fade bottom delay={500}>
                    Landing template for startups
                  </Fade>
                </h1>
                <div className="container-xs">
                  <p className="mt-0 mb-32">
                    <Fade bottom big delay={1000}>
                      Our landing page template works on all devices, so you
                      only have to set it up once, and get beautiful results
                      forever.
                    </Fade>
                  </p>
                </div>
              </div>
              <div className="hero-figure illustration-element-01">
                <Fade bottom>
                  <img
                    className="has-shadow"
                    src={MainImage}
                    alt="Hero image"
                    width="896"
                    height="504"
                  />
                </Fade>
              </div>
            </div>
          </div>
        </Section>
        <Section className="feature-tiles">
          <div className="container">
            <div className="features-tiles-inner section-inner">
              <div className="tiles-wrap">
                {features.map((features, index) => (
                  <Features
                    key={index}
                    delayInMS={index * 500}
                    data={features}
                  ></Features>
                ))}
              </div>
            </div>
          </div>
        </Section>
        
      </main>
      <Footer></Footer>
    </div>
  );
}
